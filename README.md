## Deploy Automático
    

### O que é e por quê usar  

Deploy é o processo de implementação de software, no caso de projetos web, significa configurar um servidor ou hospedagem compartilhada, para que o projeto rode da maneira esperada.

Realizar um deploy manual pode trazer uma série de problemas, além do mais natural, que é a baixa eficiência de se continuar perdendo um tempo precioso da equipe em tarefas que podem ser automatizadas. Além disso uma aplicação web pode possuir milhares de arquivos dentro de centenas de pastas, ocorrendo commits constantes. Realizar manualmente uma atualização em um sistema como esse aumenta muito a chance de erros. Um arquivo em local diferente pode inviabilizar todo o funcionamento.

O deploy automatizado vem justamente para contornar esses problemas. Optando por uma ferramenta de deploy automatizado, basta configurá-lo uma vez para realizar todas as suas atualizações sem se preocupar com nenhuma configuração, apenas com o seu código. A automatização permite que a rotina de deploy fique extremamente simples, bastando um clique para atualizar a sua aplicação com novos códigos. Nada de ficar navegando em cada pasta para subir um arquivo.
 
### Pipelines

Pipelines são o componente motriz de toda a ideia de Integração, Entrega e Implantação Contínuas.

Pipelines consistem em:

- **Jobs**, que definem **o que** fazer.
- **Stages**, que definem **quando** executar os Jobs.

Se todos os Jobs de um Stage acontecerem corretamente, o pipeline passa para o próximo Stage. No entanto, se qualquer Job em um Stage falhar, o próximo Stage, geralmente, não é executado e o pipeline é encerrado sumariamente.

Em geral, os pipelines são executados automaticamente e não requerem intervenção depois de criados. No entanto, a depender do script, pode vir a ter necessidade de ação humana.

Existem 3 formas principais de se estruturar o pipeline, cada uma com suas vantagens. Esses métodos ainda podem ser misturados.

- **Básico:** Ideal para projetos mais simples. Executa tudo em cada Stage simultaneamente, seguindo para o próximo Stage ao fim.
- **DAG(Directed Acyclic Graph ):** Voltado para projetos maiores e complexos que precisam de uma execução eficiente. 
- **Pai-Filho:** Focado em projetos com muitos componentes independentes. Ele divide pipelines complexos em um pipeline ‘Pai’, que pode acionar vários sub-pipelines ‘Filhos’, simplificando o todo.

É possível acessar o histórico de pipelines executados na página **CI/CD > Pipelines** do seu projeto. Clicando no pipeline abre a página com seus detalhes, mostrando os Jobs que rodaram na sua aplicação. Nessa seção é possível cancelar um pipeline que esteja rodando, reiniciar um que tenha falhado ou simplesmente excluir um pipeline.

### Configurando Endereço no Servidor

Nos deploys automáticos da InfoJr, utilizamos os subdomínios do servidor. Subdomínios são ramificações no seu domínio principal para diferentes seções em seu website.

As etapas são:
- Acessar o https://vault.bitwarden.com/#/lock para pegar o acesso ao cloudflare 
- No https://dash.cloudflare.com/ acessar o dominio desejado (no caso inforjr) 
- Ir na aba DNS > adicionar registro, tipo: CNAME, Nome: (Igual ao da pasta criada no server da Info)


### .gitlab-ci.yml

É um arquivo YAML onde se configura instruções específicas para poder usar o GitLab CI/CD. 

Nesse arquivo você define:
- O script que você quer rodar
- Os comandos que você quer executar em sequência e os que vai executar em paralelo
- A localização para fazer o deploy do projeto
- Definir se irá rodar o script de forma automática ou por um gatilho

O script é agrupado em Jobs. Por sua vez é possível agrupar múltiplos Jobs em Stages que rodam em uma ordem definida. Deve-se organizar os Jobs em uma sequência lógica que siga a ordem correta do que se deseja que aconteça.  

O GitLab CI/CD não apenas executa os Jobs, como também mostra o que está acontecendo durante a execução, como se estivesse vendo no seu terminal. É possível visualizar o status do pipeline pelo GitLab, e ainda é possível fazer um rollback  caso tenha ocorrido algum problema.

 
#### O .gitlab-ci.yml da Info

São utilizados dois Jobs, o Build e o Deploy, que tem Stages com mesmo nomes.

![Alt text](https://i.imgur.com/BoynsbQ.png "Build")

O Build é responsável em instalar os modules, assim como pegar os códigos do projeto através do comando run build. Tem como tempo limite para o seu processo, a marca definida de 1 hora.

![Alt text](https://i.imgur.com/WPz5CWK.png "Deploy")

No Deploy ocorre todas as etapas para subir o projeto atualizado para o servidor. Utiliza das chaves do usuário deployer do servidor, que precisam ser pré-cadastradas na página do projeto no GitLab, para fazer a conexão ssh e permitir as transferências dos arquivos gerados na build. 
